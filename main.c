/*
 * File:   main.c
 * Author: Jesu
 *
 * Created on May 22, 2018, 1:50 PM
 */

#include "monitor.h"

void main(void) {
    
    uartInit(UART_BAUD); // Initialize UART @ 9600 baud.
   
    i2cMasterInit(I2C_FREQ);
    
    state = INIT;

    input[0] = 0x01;
    TIMER2Init(); //TIMER 2
    
    while(1){
        switch (state){
            case RTC_MENU: // Just read the RTC and print the time.
                ret=(uint8_t) readRTC(currentTime);
                if(ret!=0)
                {
                    uartWriteBytes("RTC NO WORK",11);
                }
                else
                {
                    uartWriteBytes("RTC: ",5);
                    uartWriteBytes(currentTime,4);
                    print_hex_byte(currentTime[0]);
                    print_hex_byte(currentTime[1]);
                    print_hex_byte(currentTime[2]);
                    print_hex_byte(currentTime[3]);
                    uartWriteBytes("\n",1);
                }
                state = MAIN_MENU; // Quit the current state. 
                break;
                
            case INIT:
                CLEAR_COMMAND();
                CLEAR_EXTRADATA();
                ED_size = 0; 
                if (input[0] == 0x01){
                uartWriteBytes("\nTENKOH external monitor\n\n\0", 27);    
                uartWriteBytes("\"s\" to continue\n\n\0", 18);            
                }
                input[0] = 0x00;
                while (input[0] == 0x00){                
                    uartRead(input);
                    switch (input[0]){
                        case 0x73:
                            state = MAIN_MENU;
                            break;
                        default:
                            input[0] = 0x00;
                    }
                }
                break;
            
            case MAIN_MENU:
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61:               //a
                            state = EPS_MENU;       //go to EPS Menu PIC_M
                            address = picMAddr;
                            break;
                        case 0x62:         //b
                            state = COMM_MENU;     //go to COMM Menu with CCU1
                            address = ccu1Addr;
                            break;
                        case 0x63:         //c
                            state = OBC_MENU;      //go to OBC Menu with MCU
                            address = mcuDebugAddr;        
                            break;
                        case 0x64:         //d
                            state = RTC_MENU;              //go to RTC Menu
                            // RTC address is defined in DS1340.h and fixed.        
                            break;
                        case 0x65:         //e
                            state = OBC_MENU;      //go to OBC Menu with BCU
                            address = bcuDebugAddr;        
                            break;
                        case 0x66:         //f
                            state = COMM_MENU;    //go to COMM Menu with CCU2
                            address = ccu2Addr;
                            break;
                        case 0x68:         //h
                            state = EPS_MENU;    //go to COMM Menu with PIC_B
                            address = picBAddr;
                            break;
                        case 0x78:         // the x
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default: 
                            input[0] = 0x00;                         
                    }
                }
                break; //case MAIN_MENU

            case EPS_MENU:
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61:           //a
                            command[0] = D_EPS_GPIOCOM;   //Set
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;          
                            break;
                        case 0x62:     //b
                            command[0] = C_SETON;       //enable load
                            state = LOADSW;             //go to state to define which load
                            break;             
                        case 0x63:    //c
                            command[0] = C_SETOFF;      //disable load 
                            state = LOADSW;             //go to state to define which load
                            break;
                        case 0x64:    //d
                            command[0] = C_SETRST;      //reset load
                            state = LOADSW;             //go to state to define which load
                            break;
                        case 0x65:    //e
                            command[0] = C_TM_FREQ;     //define command for housekeeping saving frequency in EPS controller
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = FREQ;               //go to state to define frequency
                            break;  
                        case 0x66:    //f
                            command[0] = C_OBC_TICK;
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break;
                        case 0x67:    //g
                            command[0] = C_KILL;        //define command for Tenkoh death
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to define mode
                            break;
                        case 0x68:    //h
                            command[0] = C_NO_KILL;     //define command for Tenkoh resurrection 
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to define mode
                            break;                    
                        case 0x69:    //i
                            command[0] = D_SATRST;      //define command for full reset ***Gotta go to Normal Mode also
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to to send it
                            break;
                        case 0x6A:    //j
                            command[0] = D_GPIO_OBC;    //define command for full OBC interruption status bite change
                            state = HI_LO;              //go to state to define ON/OFF
                            break;
                        case 0x6B:    //k
                            command[0] = D_EPS_OCPADC;      //
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to send it
                            break;
                        case 0x6C:    //l
                            command[0] = D_EPS_IFSWADC;      //
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to send it
                            break;    
                        case 0x6D:    //m
                            command[0] = D_EPS_RDSBUSADC;      //
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to send it
                            break; 
                        case 0x6E:    //n
                            command[0] = D_EPS_RDSPLADC;      //
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to send it
                            break; 
                        case 0x6F:    //o
                            command[0] = D_OBC_SD_TEST;      //
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC;             //go to state to send it
                            break;    
                        case 0x70:    //p
                            command[0] = D_OBC_PIC_RST;      //
                            state = HI_LO;             //go to state to send it
                            break;      \
                        case 0x78:
                            input[0] = 0x01;
                            state = INIT;
                            break; 
                        default:
                            input[0] = 0x00;
                    }                    
                }
                uartWrite(SINGLE);
                break;//case EPS_MENU
                
            case COMM_MENU:
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61:          //a 
                            command[0] = C_TRCW;        //beacon transmission
                            ED_size = ED_max_size;
                            memcpy(ExtraData, BeaconData, sizeof(BeaconData));
                            state = SENREC;             //go to state to to send it
                            break;
                        case 0x62:    //b
                            command[0] = C_TRPACK;      //package transmission
                            ED_size = ED_max_size;
                            memcpy(ExtraData, PacketData, sizeof(PacketData));
                            state = SENREC;             //go to state to to send it
                            break;
                        case 0x63:    //c
                            command[0] = C_TRACK;       //acknowledge transmission
                            ED_size = ED_max_size;
                            memcpy(ExtraData, ackData, sizeof(ackData));
                            state = SENREC;             //go to state to to send it
                            break;
                        case 0x64:    //d
                            command[0] = D_SATRST;       //satellite reset
                            ED_size = 0; 
                            state = HI_LO;
                            break;
                        case 0x65:    //e
                            command[0] = D_GPIO_OBC;    //packet OBC interrupt
                            ED_size = 0; 
                            state = HI_LO;
                            break;
                        case 0x66:  //f        Read slave's buffer
                            command[0] = C_OBC_TICK;
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break;
                        case 0x67:    //g
                            command[0] = C_TRSNG;       //Digi Singer Mode
                            ED_size = 0; 
                            state = HI_LO;
                            break;   
                        case 0x78: // x
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default:
                            input[0] = 0x00;
                    }
                }
                uartWrite(SINGLE);
                break; //case COMM_MENU

            case OBC_MENU:
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61: //a 
                            command[0] = D_OBC_PIC_SEL;     //select PIC
                            ExtraData[0]='A'; // Brutal hack. CMD will not be received by the OBC if SUP byte is 0x00. By sending a dummy OPT byte OBC will always process the CMD.
                            ED_size = 1;
                            state = HI_LO; // Need to choose SUP data i.e. command[1]
                            break;
                        case 0x62: //b
                            command[0] = D_OBC_PIC_RST;
                            ExtraData[0]='A'; // Brutal hack. CMD will not be received by the OBC if SUP byte is 0x00. By sending a dummy OPT byte OBC will always process the CMD.
                            ED_size = 1;
                            state = HI_LO; // Need to choose SUP data i.e. command[1]
                            break;
                        case 0x63: //c
                            command[0] = D_OBC_SD_TEST;     //run SD card test
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break; 
                        case 0x64:  //d        Read the temperatures 
                            command[0] = D_OBC_T_TEST;
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break;
                        case 0x65:  //e        Read the GPIO status pins of EPS and CCUs 
                            command[0] = D_OBC_RD_INT;
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break;
                        case 0x66:  //f        Read slave's buffer
                            command[0] = C_OBC_TICK;
                            command[1]='A'; // Dummy SUP data.
                            ED_size = 0;
                            state = SENREC; // No need for SUP or OPT data - straight to sending.
                            break;
                        case 0x67:  //g        SPI test
                            command[0] = D_OBC_SND_SPI;
                            // SUP byte = slave index. Chosen in SPI_SLAVE state.
                            ExtraData[0]=D_OBC_SND_SPI; // Command to send to the CCU2 on SPI from the OBC.
                            ExtraData[1]=0x00; // SUP byte for the SPI command.
                            ExtraData[2]='S'; // From here until the end - OPT data for the SPI command.
                            ExtraData[3]='P';
                            ExtraData[4]='I';
                            ExtraData[5]='M';
                            ExtraData[6]='A';
                            ExtraData[7]='S';
                            ExtraData[8]='T';
                            ExtraData[9]='E';
                            ExtraData[10]='R';                            
                            ED_size = 11;
                            state = SPI_SLAVE; // Go to choose SUP byte.
                            break;
                        case 0x68: //h 
                            command[0] = D_OBC_RTC_EN;     //select PIC
                            ExtraData[0]='A'; // Brutal hack. CMD will not be received by the OBC if SUP byte is 0x00. By sending a dummy OPT byte OBC will always process the CMD.
                            ED_size = 1;
                            state = HI_LO; // Need to choose SUP data i.e. command[1]
                    }
                } 
                uartWrite(SINGLE);
                break; //case OBC_MENU
            
            case LOADSW: 
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61: //a 
                            command[1] = COM1_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x62://b
                            command[1] = COM2_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x63://c
                            command[1] = UCP_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x64://d
                            command[1] = PL_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x65://e
                            command[1] = DLP_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x66://f
                            command[1] = CPD_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x67://g
                            command[1] = ADS_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x68://h
                            command[1] = HEAT_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x69://i
                            command[1] = PIC_SENSE; //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x6A://j
                            command[1] = SD_CARD;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x6B://k
                            command[1] = HEATER1_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x6C://l
                            command[1] = HEATER2_SW;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x7A://z
                            command[1] = OBC_SW;    //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x78://x back to menu
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default:
                            input[0] = 0x00;
                    }
                }
                uartWrite(SINGLE);
                break; //case LOADSW

            case TICK: 
                TIMERCount(multiplier); // This one receives a multiplier and
                Ticking();
                break;

            case HI_LO: 
                Menus();
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x61: //a //HIGH
                            command[1] = C_SETON;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x62://b //LOW
                            command[1] = C_SETOFF;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x78://x back to menu
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default:
                            input[0] = 0x00;                    
                    }
                }
                break;
            
            case SPI_SLAVE: // Choose between OBC's SPI slaves, i.e. CCU1, CCU2 and EPS Controller.
                Menus();
                while (input[0] == 0x00){
                    uartRead(input); //
                    switch (input[0]){
                        case 0x61: //a // CCU1
                            command[1] = 0;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x62://b // CCU2
                            command[1] = 1;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x63://c // EPS
                            command[1] = 2;   //define second part of command
                            state = SENREC;         //go to state to send it
                            break;
                        case 0x78://x back to menu
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default:
                            input[0] = 0x00;  
                    }
                        
                    }                
                break;
                
            case SENREC: // Exchange data with the Ten-Koh PIC on I2C.
                uartWriteBytes("Command", 7);
                uartWriteBytes(&address, 1);
                uartWrite(SINGLE);

                packACommand(command[0], command[1], ExtraData, ED_size);
                exchangeDataI2C(&address, 2 + ED_size); // CMD byte + SUP byte + OPT bytes, if any.

                uartWriteBytes(bufferTransmit, sizeof(bufferTransmit));
                
                __delay_ms(100);
                switch (address){
                    case 0x10:  //EPS PIC_M address
                    case 0x20:  //EPS
                        uartWriteBytes("Buffer\n",7);
                        uartWriteBytes(bufferReceive, sizeof(bufferReceive));
                        uartWriteBytes("\nAnswer\n",8);
                        PrintAnswer(bufferReceive, 16, 3);
                        uartWriteBytes("\nHex:\n",6);
                        //Print the HEX into ASCII including first byte
                        print_data_1_array(0,bufferReceive[0]+1,bufferReceive);
                        //PrintAnswerEPS(bufferReceive, 10, 3);
                        //IncludeInBeacon(BeaconData, bufferReceive,10,0);                        
                    break;
                    case 0x30:  //COMM1
                        PrintAnswer(bufferReceive, 6, 3);
                        uartWrite(SINGLE);
                        uartWriteBytes(bufferReceive, sizeof(bufferReceive));
                    break;
                    case 0x40:  //COMM2
                        PrintAnswer(bufferReceive, 6, 3);
                        uartWrite(SINGLE);
                        uartWriteBytes(bufferReceive, sizeof(bufferReceive));
                    break;
                    case 0xF0:  //BCU
                    case 0xB0:  //MCU - two cases after each other will work like an or (MCU or BCU address).
                        PrintAnswer(bufferReceive, 6, 3);
                        uartWrite(SINGLE);
                        uartWriteBytes(bufferReceive, sizeof(bufferReceive));
                    break;
                }
                
                uartWriteBytes(DOUBLE,2);
                uartWriteBytes("\"x\" for main menu\n\n", 19);
                input[0] = 0x00;
                while (input[0] == 0x00){
                    uartRead(input);
                    switch (input[0]){
                        case 0x78:
                            input[0] = 0x01;
                            state = INIT;
                            break;
                        default:
                             input[0] = 0x00;                            
                    }
                } 
                break;
        }
    }
}
