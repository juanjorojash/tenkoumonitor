/*
 * File:   monitor.c
 * Author: JuanJ0
 *
 * Created on June 7, 2018, 9:40 PM
 */

#include "monitor.h"

/**************************************************************************************
 * TIMER2Init
 *
 * Configure the timer 2 module usign a 1:16 postscaler and a 16 prescaler and
 * a period register of 250 instruction cycles to get a total of 12.8 ms cycles.
 * 
 **************************************************************************************/

void TIMER2Init(void){
    //TOUTPS3:TOUTPS0 = 1111,  1:16 Postscaler
    TOUTPS3 = 1;
    TOUTPS2 = 1;
    TOUTPS1 = 1;
    TOUTPS0 = 1;
    TMR2ON = 1; //Timer 2 ON
    //T2CKPS1:T2CKPS0 = 11, 16 Prescaler
    T2CKPS1 = 1;
    T2CKPS0 = 1;
    PR2 = 0b11111010; //250 
    //This leads to 12.8 ms
}


/**************************************************************************************
 * TIMERCount
 *
 * Set the SEC_FLAG byte every "fmult * 0.9984s". This value comes form 78 * 12.8ms = 998.4ms
 * Check TIMER2Init for additional information. 
 *
 * Attributes
 * -----------
 *  * fmult: scaler to set the number of seconds of the counter. 
 **************************************************************************************/

void TIMERCount(uint8_t fmult){
    if (TMR2IF){
        count --;
        TMR2IF = 0;
    }
    if (count <= 0){
        count = (int16_t) (ONE_SEC * fmult);
        TICK_FLAG = 1;
    } 
}

/**************************************************************************************
 * PrintAnswer
 *
 * Print the received answer in the terminal, the hk bytes are just printed, the stat
 * bytes are converted in strings that represent their HEX value and then are printed
 *   
 * Attributes
 * -----------
 *  * buffer: data received as answer.
 *  * hk_size: size of the housekeeping bytes
 *  * stat_size: size of the status bytes 
 **************************************************************************************/

void PrintAnswer(unsigned char* buffer, unsigned char hk_size, unsigned char stat_size){
    unsigned char temp[20] = {0};
    char temp2[20] = {0};
    unsigned char size;
    uint8_t i = 0; 
    size = hk_size + stat_size;    
    for (i = 1; i <= size; i++){
        if (i <= hk_size){
            memset(temp,0,sizeof(temp));
            temp[i-1] = buffer[i];
            uartWriteBytes(temp, sizeof(temp));          
        }else{
            memset(temp2,0,sizeof(temp2));
            utoa(temp2, buffer[i], 16);
            if (buffer[i] < 0x16){
                temp2[1] = temp2[0];
                temp2[0] = '0';
            } 
            uartWriteBytes(temp2, sizeof(temp2));
                        
        }        
    }
    uartWrite(SINGLE);
}

/**************************************************************************************
 * TempConvPrint
 *

 *   
 * Attributes
 * -----------
 *  * temper1: first hex of temperature
 *  * temper2: second hex of temperature
 **************************************************************************************/

void TempConvPrint(unsigned char temper1, unsigned char temper2){
    float temper;
    char* ft_str;
    int status; 
    temper = (((temper1 - 48) * 10) + (temper2 - 48));
    temper = temper * 7.3529; //K
    temper = temper - 273.15; //C
    ft_str = ftoa(temper, &status);
    if (temper >= 100){
        uartWriteBytes(ft_str, 5);
    }else{
        uartWriteBytes(ft_str, 4);
    }    
}


/**************************************************************************************
 * PrintAnswerEPS
 *
 * Print the received answer in the terminal, the hk bytes are just printed, the stat
 * bytes are converted in strings that represent their HEX value and then are printed.
 * 
 * After that the telemetry and status of the loads is shown in a more user-friendly way 
 *   
 * Attributes
 * -----------
 *  * buffer: data received as answer.
 *  * hk_size: size of the housekeeping bytes
 *  * stat_size: size of the status bytes 
 **************************************************************************************/

void PrintAnswerEPS(unsigned char* buffer, unsigned char hk_size, unsigned char stat_size){
    unsigned char temp[10] = {0};
    char temp2[10] = {0};
    unsigned char size;
    unsigned char const hk_title[] = {"TBAT1\tTBAT2\tVBAT\tIBAT1\tIBAT2\n"};
    unsigned char const stat1_title[] = {"COM1\tCOM2\tUCP\tPL\tDLP\tCPD\tADS\tHEAT\n"};
    unsigned char const stat2_title[] = {"PSENS\tSD\tOBC\n"};
    unsigned char const rece_str[] = {"Received data:"};
    unsigned char const ON[] = {"ON"};
    unsigned char const OFF[] = {"OFF"};
    uint8_t i = 0;
    size = hk_size + stat_size; 
    //PRINT FIRST WHAT WAS RECEIVED    
    uartWriteBytes(rece_str, sizeof(rece_str));  
    for (i = 1; i <= size; i++){
        if (i <= hk_size){
            memset(temp,0,sizeof(temp));
            temp[i-1] = buffer[i];
            uartWriteBytes(temp, sizeof(temp));          
        }else{
            memset(temp2,0,sizeof(temp2));
            utoa(temp2, buffer[i], 16);
            if (buffer[i] < 0x16){
                temp2[1] = temp2[0];
                temp2[0] = '0';
            } 
            uartWriteBytes(temp2, sizeof(temp2));
                        
        }        
    }
    uartWriteBytes(DOUBLE, 2);
    //Now print HK data
    uartWriteBytes(hk_title, sizeof(hk_title));
    for (i = 1; i <= hk_size; i = i + 2){
        TempConvPrint(buffer[i], buffer[i+1]);
        if (i == hk_size){
            uartWriteBytes(DOUBLE, 2);
        }else{
            uartWrite(TAB);
        }      
    }
    uartWriteBytes(DOUBLE, 2);
    //Now print status data
    uartWriteBytes(stat1_title, sizeof(stat1_title));
    for (i = 0; i <= 7; i++){
        if (i == 0){
            if(((buffer[size-1]) & 0x80) == 0x80){
                uartWriteBytes(ON, sizeof(ON));
            }else{
                uartWriteBytes(OFF, sizeof(OFF));
            }
        }else{
            if(((buffer[size-1] << i) & 0x80) == 0x80){
                uartWriteBytes(ON, sizeof(ON));
            }else{
                uartWriteBytes(OFF, sizeof(OFF));
            }
        }

        if (i == 7){
            uartWriteBytes(DOUBLE, 2);
        }else{
            uartWrite(TAB);
        }        
    }
    //SECOND SET OF LOADS
    uartWriteBytes(stat2_title, sizeof(stat2_title));
    for (i = 0; i <= 2; i++){
        if (i == 0){
            if(((buffer[size]) & 0x80) == 0x80){
                uartWriteBytes(ON, sizeof(ON));
            }else{
                uartWriteBytes(OFF, sizeof(OFF));
            }
        }else{
            if(((buffer[size] << i) & 0x80) == 0x80){
                uartWriteBytes(ON, sizeof(ON));
            }else{
                uartWriteBytes(OFF, sizeof(OFF));
            }  
        }

        if (i == 2){
            uartWrite(SINGLE);
        }else{
            uartWrite(TAB);
        }        
    }
}

void IncludeInBeacon(unsigned char* beacon, unsigned char* toinc, unsigned char inc_size, unsigned char in_pos){
    uint8_t i = 0;
    for (i = in_pos;  i < in_pos + inc_size; i++){
        //beacon[i] = toinc[i - in_pos];      
    }
}

void Menus(void){
    unsigned char const     chos_str[10] = "Choose:\n\n\0";
    //unsigned char const     main_menu_str[19] = "a)EPS\tb)COMM\tc)OBC";
    //unsigned char const     eps_menu_str1[35] = "a)One tick\t\t\tb)Enable\t\t\tc)Disable\n\0";
    //unsigned char const     eps_menu_str2[43] = "d)Reset\t\t\te)HK save period\t\tf)HK history\n\0";
    //unsigned char const     eps_menu_str3[33] = "g)Kill\t\th)Resurrect\t\ti)Sat Reset\n\0";
    //unsigned char const     eps_menu_str4[24] = "j)OBC GPIO\t\tk)Ten ticks\0";
    unsigned char const     comm_menu_str[] = "a)Beacon\t\tb)Package\t\tc)Acknowledge\nd)Sat Reset\t\te)OBC Interrupt\t\tf)OBC Tick\ng)SNG\0";
    unsigned char const     obc_menu_str1[] = "a)Select PIC\t\tb)ON/OFF PIC\t\tc)SD card test\n\0";
    unsigned char const     obc_menu_str2[] = "d)Read temps\t\te)Read GPIOs\t\tf)Send OBC Tick\ng)SPI test\th)Enable RTC\0";
    unsigned char const     freq_str[] = "a)1\tb)5\tc)10\nd)20\te)30\tf)60";
    unsigned char const     load_str[] = "a)COMM1\tb)COMM2\tc)UCP\nd)PL\te)DLP\tf)CPD\ng)ADS\th)HEAT\ti)PICSENS\nj)SD\tk)Heat1\tl)Heat2\nz)OBC";
    unsigned char const     tick_str[] = "a)1s\tb)5s\tc)10s\nd)20s\te)30s\tf)60s";
    unsigned char const     hilo_str[] = "a)HIGH/ON\tb)LOW/OFF";
    unsigned char const     spiSlave_str[] = "a)CCU1\tb)CCU2\tc)EPS";

    switch(state){
        case MAIN_MENU:
            uartWriteBytes("Choose:\n\n\0", 10);
            uartWriteString("a)EPS-PIC_M\tb)COMM-CCU1\tc)OBC-MCU\td)RTC\n");
            uartWriteString("e)OBC-BCU\tf)COMM-CCU2\tg)*******\th)EPS-PIC_B");
            break;
        case EPS_MENU:
            uartWriteString("Choose:\n\n\0");
            uartWriteString("a)GPIO COM\t\tb)Enable\t\tc)Disable\n\0");  
            uartWriteString("d)Reset\t\t\te)HK save period\tf)ticks\n\0");  
            uartWriteString("g)Kill Switch\t\th)Resurrect\t\ti)Sat Reset\n\0");  
            uartWriteString("j)OBC GPIO\t\tk)ADC OCP\t\tl)ADC IFSW\n");  
            uartWriteString("m)ADC BUS\t\tn)ADC PL\t\to)SD card\n");
            uartWriteString("p)PIC reset\n");
            break;
        case COMM_MENU:
            uartWriteBytes("Choose:\n\n\0", 10);
            uartWriteBytes(comm_menu_str, sizeof(comm_menu_str));
            break;
        case OBC_MENU:
            uartWriteBytes("Choose:\n\n\0", 10);
            uartWriteBytes(obc_menu_str1, sizeof(obc_menu_str1));
            uartWriteBytes(obc_menu_str2, sizeof(obc_menu_str2));
            break;
        case LOADSW:
            uartWriteBytes(chos_str, sizeof(chos_str));
            uartWriteBytes(load_str, sizeof(load_str));
            break;
        case FREQ:
            uartWriteBytes(chos_str, sizeof(chos_str));
            uartWriteBytes(freq_str, sizeof(freq_str));  
            break;
        case TICK_FREQ:
            uartWriteBytes(chos_str, sizeof(chos_str));
            uartWriteBytes(tick_str, sizeof(tick_str));
            break;
        case HI_LO:
            uartWriteBytes(chos_str, sizeof(chos_str));
            uartWriteBytes(hilo_str, sizeof(hilo_str));
            break;
        case SPI_SLAVE:
            uartWriteBytes(chos_str, sizeof(chos_str));
            uartWriteBytes(spiSlave_str, sizeof(spiSlave_str));
    }
    uartWriteBytes(DOUBLE, 2);
    input[0] = 0x00;
}

void Ticking(void){
    if (TICK_FLAG){
        packACommand(command[0],command[1],ExtraData,ED_size);
        exchangeDataI2C(&address, CMD_size + ED_size);
        PrintAnswer(bufferReceive, 10, 3);
        if (tick_cycles <= 0){
            tick_cycles = 10;
            input[0] = 0x01;
            state = INIT;
            uartWrite(SINGLE);
        }else{
            tick_cycles--;
            state = TICK;
        }
        TICK_FLAG = 0;
    }
}

// void IncludeInBeacon(unsigned char* beacon, unsigned char* toinc, unsigned char inc_size, unsigned char in_pos){
//     for (i = in_pos;  i < in_pos + inc_size; i++){
//         if (i == 0){
//             command[1] = toin[i - in_pos]
//         }else{
//             beacon[i - 1] = toin[i - in_pos];
//         }        
//     }
// }
void print_hex_byte( unsigned char data ) //Function from DIMA branch uart.c
{
	const unsigned char disp_table[] = "0123456789ABCDEF";

	uartWrite( disp_table + (( data & 0xf0 ) >> 4 ));
	uartWrite( disp_table + ( data & 0xf ));
}
//Function from DIMA branch uart.c
void print_data_1_array(unsigned int byte_number, unsigned char data_size, unsigned char* data)
{
    // Transmit the read data over UART to see if they make sense.
    uartWriteBytes(" ",1); 
    for(unsigned char i=byte_number; i<byte_number+data_size; i++){
        print_hex_byte(data[i]);
    }
    uartWriteBytes("\n\r",2);
}