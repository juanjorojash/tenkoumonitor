/* Ten-Koh Monitor
 * Intended for use with PIC16F877
 */
#ifndef MONITOR_H
#define	MONITOR_H

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#define _XTAL_FREQ     20000000

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>
#include <stdint.h> //To use uint_16, etc.
#include "i2c_master.h"
#include "uart.h"
#include "OnBoardCommands.h"
#include "I2CAddresses.h"
#include "TK-01-01-01_OBC.h"
#include  "DS1340.h"


enum states { INIT = 0, MAIN_MENU = 1, EPS_MENU = 2, COMM_MENU = 3, OBC_MENU = 4,
    RTC_MENU = 5, SENREC = 6, LOADSW = 7, TICK_FREQ = 8, FREQ = 9, TICK = 10,
    HI_LO = 11, SPI_SLAVE = 12};


#define SINGLE  "\n"
#define DOUBLE 	"\n\n"
#define TAB		"\t"

#define UART_RX RC7 // Rx
#define UART_TX RC6 // Tx
#define UART_RX_INIT TRISC7
#define UART_TX_INIT TRISC6

// System-wide I2C, using the in-built PIC module.
#define I2C_SCL RC3
#define I2C_SDL RC4
#define I2C_SCL_INIT TRISC3
#define I2C_SDL_INIT TRISC4

#define UART_BAUD 9600 // Bits per second.
#define I2C_FREQ 100000 // I2C frequency in Hz.
#define ONE_SEC 78
#define CMD_size 2
#define ED_max_size 20 //Check this size later
#define CLEAR_COMMAND() {memset(command,0,CMD_size);}
#define CLEAR_EXTRADATA() {memset(ExtraData,0,ED_max_size);}


uint8_t                 multiplier = 1;
uint8_t                 tick_cycles = 10;
int16_t                 count = 78;
unsigned char           ASCIImult = 0;
unsigned char           TICK_FLAG; 
unsigned char           state;
unsigned char           address = 0x00;
unsigned char           command[CMD_size] = {0};
unsigned char           input[1] = {0};
unsigned char           ED_size = 0;
unsigned char           ExtraData[ED_max_size] = {0}; //need to check size 
unsigned char const          BeaconData[13] = {"DUMMY BEACON"};
unsigned char const          PacketData[13] = {"DUMMY PACKET"};
unsigned char const          ackData[10] = {"DUMMY ACK"};
//unsigned char const     init_str[27] = "\nTENKOH external monitor\n\n\0";
//unsigned char const     pres_str[18] = "\"s\" to continue\n\n\0";
//unsigned char const     chos_str[10] = "Choose:\n\n\0";
//unsigned char const     comm_str[] = "Command...\0";
//unsigned char const     tickcom_str[16] = "Tick command...\0";
//unsigned char const     prsx_str[] = "\"x\" for main menu\n\n";



void TIMER2Init(void);
void TIMERCount(uint8_t fmult);
void PrintAnswer(unsigned char* buffer, unsigned char hk_size, unsigned char stat_size);
void TempConvPrint(unsigned char temper1, unsigned char temper2);
void PrintAnswerEPS(unsigned char* buffer, unsigned char hk_size, unsigned char stat_size);
void IncludeInBeacon(unsigned char* beacon, unsigned char* toinc, unsigned char inc_size, unsigned char in_pos);
void Menus(void);
void Ticking(void);
void print_hex_byte( unsigned char data );
void print_data_1_array(unsigned int byte_number, unsigned char data_size, unsigned char* data);

#endif	/* MONITOR_H */

